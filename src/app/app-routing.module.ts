import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TodoRoutes } from './todo/todo.routing';


const routes: Routes = [];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    TodoRoutes
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
