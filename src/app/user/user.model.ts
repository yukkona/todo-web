import { Todo } from '../todo/todo.model';

export class User {
  constructor(
    public createdAt: Date,
    public updatedAt: Date,
    public username: string,
    public id: number,
    public fullname: string,
    public todos: Todo[]
  ) {}
}
