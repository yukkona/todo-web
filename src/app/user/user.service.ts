import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './user.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private apiUrl: string;
  constructor(private http: HttpClient) {
    this.apiUrl = environment.apiUrl;
  }

  getAll() {
    return this.http.get<User[]>(`${this.apiUrl}/user`).toPromise();
  }

  getOne(id: number) {
    return this.http.get<User>(`${this.apiUrl}/user/${id}`).toPromise();
  }
}
