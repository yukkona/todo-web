import { User } from '../user/user.model';

export class Todo {
  constructor(
    public text: string,
    public isDone: boolean = false,
    public isDeleted: boolean = false,
    public id: number = -1,
    public owner?: User,
    public createdAt?: Date,
    public updatedAt?: Date,
  ) {  }
}
