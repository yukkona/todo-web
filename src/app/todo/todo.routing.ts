import { Routes, RouterModule } from '@angular/router';
import { TodoListComponent } from './todo-list/todo-list.component';
import { TodoDetailsComponent } from './todo-details/todo-details.component';
import { TodoAddComponent } from './todo-add/todo-add.component';
import { TodoEditComponent } from './todo-edit/todo-edit.component';

const routes: Routes = [
  { path: '', component: TodoListComponent },
  { path: 'todo/:todoId/view', component: TodoDetailsComponent },
  { path: 'todo/:todoId/edit', component: TodoEditComponent },
  { path: 'todo/new', component: TodoAddComponent }
];

export const TodoRoutes = RouterModule.forChild(routes);
