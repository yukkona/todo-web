import { Component, OnInit } from '@angular/core';
import { TodoService } from '../todo.service';
import { UserService } from 'src/app/user/user.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from 'src/app/user/user.model';
import { Todo } from '../todo.model';

@Component({
  selector: 'app-todo-edit',
  templateUrl: './todo-edit.component.html',
  styleUrls: ['./todo-edit.component.css']
})
export class TodoEditComponent implements OnInit {

  users: Promise<User[]>;
  model: Todo;
  todoForm: FormGroup;
  constructor(
    private todoService: TodoService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private router: Router,
    private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.todoService.getOne(Number.parseInt(params.get('todoId'), 10)).then(value => this.model = value);
    });
    this.users = this.userService.getAll();
    this.todoForm = this.formBuilder.group(this.model);
  }

  onSubmit(value) {
    this.todoService.update(this.model.id, value).then(data => {
      this.router.navigate(['/todo', data.id, 'view']);
    });
  }

}
