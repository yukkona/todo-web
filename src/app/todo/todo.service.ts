import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Todo } from './todo.model';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private apiUrl: string;
  constructor(private http: HttpClient) {
    this.apiUrl = environment.apiUrl;
  }

  getAll() {
    return this.http.get<Todo[]>(`${this.apiUrl}/todo`).toPromise();
  }

  getOne(id: number) {
    return this.http.get<Todo>(`${this.apiUrl}/todo/${id}`).toPromise();
  }

  add(todo: Todo) {
    return this.http.post<Todo>(`${this.apiUrl}/todo`, todo).toPromise();
  }

  update(id: number, updatedFields) {
    return this.http.patch<Todo>(`${this.apiUrl}/todo/${id}`, updatedFields).toPromise();
  }

  delete(id: number) {
    return this.http.delete<Todo>(`${this.apiUrl}/todo/${id}`).toPromise();
  }

}
