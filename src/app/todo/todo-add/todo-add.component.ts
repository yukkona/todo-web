import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { TodoService } from 'src/app/todo/todo.service';
import { Todo } from 'src/app/todo/todo.model';
import { UserService } from 'src/app/user/user.service';
import { User } from 'src/app/user/user.model';

@Component({
  selector: 'app-todo-add',
  templateUrl: './todo-add.component.html',
  styleUrls: ['./todo-add.component.css']
})
export class TodoAddComponent implements OnInit {
  users: Promise<User[]>;
  model: Todo = new Todo('');
  todoForm: FormGroup;
  constructor(
    private todoService: TodoService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {  }

  ngOnInit() {
    this.users = this.userService.getAll();
    this.todoForm = this.formBuilder.group(this.model);
  }

  onSubmit(todoData: Todo) {
    this.todoService.add(todoData).then(data => {
      this.router.navigate(['/todo', data.id, 'view']);
    });
  }

}
