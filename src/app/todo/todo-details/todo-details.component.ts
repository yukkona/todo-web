import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TodoService } from '../todo.service';
import { Todo } from '../todo.model';

@Component({
  selector: 'app-todo-details',
  templateUrl: './todo-details.component.html',
  styleUrls: ['./todo-details.component.css']
})
export class TodoDetailsComponent implements OnInit {

  todo: Todo;
  constructor(
    private route: ActivatedRoute,
    private todoService: TodoService,
    private router: Router
  ) {
    this.route.paramMap.subscribe(params => {
      this.todoService.getOne(Number.parseInt(params.get('todoId'), 10)).then(value => this.todo = value);
    });
  }

  ngOnInit() {
  }

  markDone() {
    this.todoService.update(this.todo.id, { isDone: true }).then(data => {
      this.router.navigate(['']);
    });
  }

  delete() {
    this.todoService.delete(this.todo.id).then(() => {
      this.router.navigate(['']);
    });
  }

}
