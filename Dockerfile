FROM node:alpine AS builder
RUN apk --no-cache --update --virtual build-dependencies add python make g++
WORKDIR /app
COPY package*.json ./
RUN npm ci
COPY . .
RUN npm run build --prod

FROM nginx:alpine
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/static.conf /etc/nginx/conf.d
WORKDIR /app
COPY --from=builder /app/dist/todo-ng .
