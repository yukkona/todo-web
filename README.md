# ToDo Angular App
[![pipeline status](https://gitlab.com/yukkona/todo-web/badges/master/pipeline.svg)](https://gitlab.com/yukkona/todo-web/-/commits/master)
[![coverage report](https://gitlab.com/yukkona/todo-web/badges/master/coverage.svg)](https://gitlab.com/yukkona/todo-web/-/commits/master)

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.4.

## The hello-world project
Этот проект был задуман как простое hello world приложение, написанное с помощью Angular и TypeScript

## Запуск
### Из Docker-образа (собранный)
Просто выполните `docker run -d -p 80:80 yukkona/todoweb:latest`

Проект будет доступен в браузере по адресу `http://your-server-ip-or-dns-name/`
### Сборка вручную
- Склонируйте этот репозиторий
- В папке с склонированным репозиторием запустите эмулятор терминала
- В эмуляторе терминала выполните `docker build -t <your-tag> .`

### Сборка в VSCode
В репозитории есть уже подготовленная задача для финальной сборки. Просто запустите задачу `docker build`.

## Disclaimer
Данный проект задумывался чисто в образовательных целях, никакой ценности в себе не несет.

Данный проект не завершен и в текущем состоянии неработоспособен, постоянно дорабатывается мной в рамках изучения Angular.
